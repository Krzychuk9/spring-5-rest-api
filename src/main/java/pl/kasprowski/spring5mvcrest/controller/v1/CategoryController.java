package pl.kasprowski.spring5mvcrest.controller.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.kasprowski.spring5mvcrest.api.v1.model.CategoryDto;
import pl.kasprowski.spring5mvcrest.api.v1.model.CategoryListDto;
import pl.kasprowski.spring5mvcrest.services.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CategoryListDto getAllCategories() {
        List<CategoryDto> categories = categoryService.getAllCategories();
        return new CategoryListDto(categories);
    }

    @GetMapping("/{name}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryDto getCategoryByName(@PathVariable String name) {
        return categoryService.getCategoryByName(name);
    }
}
