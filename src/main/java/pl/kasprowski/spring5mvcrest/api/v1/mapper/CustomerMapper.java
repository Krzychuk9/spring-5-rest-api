package pl.kasprowski.spring5mvcrest.api.v1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.kasprowski.spring5mvcrest.api.v1.model.CustomerDto;
import pl.kasprowski.spring5mvcrest.entity.Customer;

import java.util.List;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(target = "customerUrl", expression = "java(\"/shop/customer/\" + customer.getId())")
    CustomerDto customerToDto(Customer customer);

    Customer dtoToCustomer(CustomerDto dto);

    List<CustomerDto> customersToDtos(List<Customer> customers);

    List<Customer> dtosToCustomer(List<CustomerDto> dtos);
}
