package pl.kasprowski.spring5mvcrest.api.v1.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class VendorDto {
    @ApiModelProperty("Name of vendor")
    private String name;
    private String vendorUrl;
}
