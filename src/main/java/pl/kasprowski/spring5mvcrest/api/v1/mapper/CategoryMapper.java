package pl.kasprowski.spring5mvcrest.api.v1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.kasprowski.spring5mvcrest.api.v1.model.CategoryDto;
import pl.kasprowski.spring5mvcrest.entity.Category;

import java.util.List;

@Mapper
public interface CategoryMapper {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    CategoryDto categoryToDto(Category category);

    Category dtoToCategory(CategoryDto dto);

    List<CategoryDto> categoriesToDtos(List<Category> categories);

    List<Category> dtosToCategories(List<CategoryDto> dtos);
}
