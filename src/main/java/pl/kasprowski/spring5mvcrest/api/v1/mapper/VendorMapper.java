package pl.kasprowski.spring5mvcrest.api.v1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.kasprowski.spring5mvcrest.api.v1.model.VendorDto;
import pl.kasprowski.spring5mvcrest.entity.Vendor;

import java.util.List;

@Mapper
public interface VendorMapper {

    VendorMapper INSTANCE = Mappers.getMapper(VendorMapper.class);

    @Mapping(target = "vendorUrl", expression = "java(\"/api/v1/vendors/\" + vendor.getId())")
    VendorDto vendorToDto(Vendor vendor);

    Vendor dtoToVendor(VendorDto dto);

    List<VendorDto> vendrosToDtos(List<Vendor> vendros);

    List<Vendor> dtosToVendros(List<VendorDto> dtos);
}
