package pl.kasprowski.spring5mvcrest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.spring5mvcrest.api.v1.mapper.CategoryMapper;
import pl.kasprowski.spring5mvcrest.api.v1.model.CategoryDto;
import pl.kasprowski.spring5mvcrest.entity.Category;
import pl.kasprowski.spring5mvcrest.repositories.CategoryRepository;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;
    private CategoryMapper mapper;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper mapper) {
        this.categoryRepository = categoryRepository;
        this.mapper = mapper;
    }

    @Override
    public CategoryDto getCategoryByName(String name) {
        Category category = categoryRepository.findByName(name);
        return mapper.categoryToDto(category);
    }

    @Override
    public List<CategoryDto> getAllCategories() {
        List<Category> categories = categoryRepository.findAll();
        return mapper.categoriesToDtos(categories);
    }
}
