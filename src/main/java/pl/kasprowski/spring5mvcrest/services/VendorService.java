package pl.kasprowski.spring5mvcrest.services;

import pl.kasprowski.spring5mvcrest.api.v1.model.VendorDto;

import java.util.List;

public interface VendorService {

    VendorDto getVendorById(long id);

    List<VendorDto> getAllVendors();

    VendorDto createNewVendor(VendorDto dto);

    VendorDto updateVendor(Long id, VendorDto dto);

    VendorDto patchVendor(Long id, VendorDto dto);

    void deleteVendorById(Long id);
}
