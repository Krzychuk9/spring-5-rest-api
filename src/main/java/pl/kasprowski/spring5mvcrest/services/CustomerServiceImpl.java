package pl.kasprowski.spring5mvcrest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.spring5mvcrest.api.v1.mapper.CustomerMapper;
import pl.kasprowski.spring5mvcrest.api.v1.model.CustomerDto;
import pl.kasprowski.spring5mvcrest.entity.Customer;
import pl.kasprowski.spring5mvcrest.exceptions.ResourceNotFoundException;
import pl.kasprowski.spring5mvcrest.repositories.CustomerRepository;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;
    private CustomerMapper mapper;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper mapper) {
        this.customerRepository = customerRepository;
        this.mapper = mapper;
    }

    @Override
    public CustomerDto getCustomerById(long id) {
        Customer customer = customerRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        return mapper.customerToDto(customer);
    }

    @Override
    public List<CustomerDto> getAllCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return mapper.customersToDtos(customers);
    }

    @Override
    public CustomerDto createNewCustomer(CustomerDto dto) {
        Customer customer = mapper.dtoToCustomer(dto);
        return this.saveCustomer(customer);
    }

    @Override
    public CustomerDto updateCustomer(Long id, CustomerDto dto) {
        Customer customer = mapper.dtoToCustomer(dto);
        customer.setId(id);
        return this.saveCustomer(customer);
    }

    @Override
    public CustomerDto patchCustomer(Long id, CustomerDto dto) {
        return customerRepository.findById(id).map(customer -> {
            String firstName = dto.getFirstName();
            String lastName = dto.getLastName();

            if (firstName != null) {
                customer.setFirstName(firstName);
            }

            if (lastName != null) {
                customer.setLastName(lastName);
            }

            Customer savedCustomer = customerRepository.save(customer);
            return mapper.customerToDto(savedCustomer);
        }).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public void deleteCustomerById(Long id) {
        customerRepository.deleteById(id);
    }

    private CustomerDto saveCustomer(Customer customer) {
        Customer savedCustomer = customerRepository.save(customer);
        return mapper.customerToDto(savedCustomer);
    }
}
