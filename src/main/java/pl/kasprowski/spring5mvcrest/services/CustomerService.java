package pl.kasprowski.spring5mvcrest.services;

import pl.kasprowski.spring5mvcrest.api.v1.model.CustomerDto;

import java.util.List;

public interface CustomerService {

    CustomerDto getCustomerById(long id);

    List<CustomerDto> getAllCustomers();

    CustomerDto createNewCustomer(CustomerDto dto);

    CustomerDto updateCustomer(Long id, CustomerDto dto);

    CustomerDto patchCustomer(Long id, CustomerDto dto);

    void deleteCustomerById(Long id);
}
