package pl.kasprowski.spring5mvcrest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.spring5mvcrest.api.v1.mapper.VendorMapper;
import pl.kasprowski.spring5mvcrest.api.v1.model.VendorDto;
import pl.kasprowski.spring5mvcrest.entity.Vendor;
import pl.kasprowski.spring5mvcrest.exceptions.ResourceNotFoundException;
import pl.kasprowski.spring5mvcrest.repositories.VendorRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VendorServiceImpl implements VendorService {

    private VendorRepository vendorRepository;
    private VendorMapper mapper;

    @Autowired
    public VendorServiceImpl(VendorRepository vendorRepository, VendorMapper mapper) {
        this.vendorRepository = vendorRepository;
        this.mapper = mapper;
    }

    @Override
    public VendorDto getVendorById(long id) {
        return vendorRepository.findById(id)
                .map(mapper::vendorToDto)
                .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public List<VendorDto> getAllVendors() {
        return vendorRepository.findAll()
                .stream()
                .map(mapper::vendorToDto)
                .collect(Collectors.toList());
    }

    @Override
    public VendorDto createNewVendor(VendorDto dto) {
        Vendor vendor = mapper.dtoToVendor(dto);
        return this.saveVendor(vendor);
    }

    @Override
    public VendorDto updateVendor(Long id, VendorDto dto) {
        Vendor vendor = mapper.dtoToVendor(dto);
        vendor.setId(id);
        return this.saveVendor(vendor);
    }

    @Override
    public VendorDto patchVendor(Long id, VendorDto dto) {
        return vendorRepository.findById(id).map(vendor -> {
            String name = dto.getName();

            if (name != null) {
                vendor.setName(name);
            }

            Vendor saveVendor = vendorRepository.save(vendor);
            return mapper.vendorToDto(saveVendor);
        }).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public void deleteVendorById(Long id) {
        vendorRepository.deleteById(id);
    }

    private VendorDto saveVendor(Vendor vendor) {
        Vendor savedVendor = vendorRepository.save(vendor);
        return mapper.vendorToDto(savedVendor);
    }
}
