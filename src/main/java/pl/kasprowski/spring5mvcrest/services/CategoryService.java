package pl.kasprowski.spring5mvcrest.services;

import pl.kasprowski.spring5mvcrest.api.v1.model.CategoryDto;

import java.util.List;

public interface CategoryService {

    CategoryDto getCategoryByName(String name);

    List<CategoryDto> getAllCategories();
}
