package pl.kasprowski.spring5mvcrest.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.kasprowski.spring5mvcrest.entity.Category;
import pl.kasprowski.spring5mvcrest.entity.Customer;
import pl.kasprowski.spring5mvcrest.entity.Vendor;
import pl.kasprowski.spring5mvcrest.repositories.CategoryRepository;
import pl.kasprowski.spring5mvcrest.repositories.CustomerRepository;
import pl.kasprowski.spring5mvcrest.repositories.VendorRepository;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
public class Bootstrap implements CommandLineRunner {

    private CategoryRepository categoryRepository;
    private CustomerRepository customerRepository;
    private VendorRepository vendorRepository;

    @Autowired
    public Bootstrap(CategoryRepository categoryRepository, CustomerRepository customerRepository,
                     VendorRepository vendorRepository) {
        this.categoryRepository = categoryRepository;
        this.customerRepository = customerRepository;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        loadCategories();
        loadCustomers();
        loadVendors();
    }

    private void loadVendors() {
        Set<Vendor> vendros = new HashSet<>();

        Vendor vendor1 = new Vendor();
        vendor1.setName("Western Tasty Fruits Ltd.");
        vendros.add(vendor1);

        Vendor vendor2 = new Vendor();
        vendor2.setName("Exotic Fruits Company");
        vendros.add(vendor2);

        Vendor vendor3 = new Vendor();
        vendor3.setName("Home Fruits");
        vendros.add(vendor3);

        vendorRepository.saveAll(vendros);
        log.info("Data loaded! Number of vendors: " + vendorRepository.count());
    }

    private void loadCustomers() {
        Set<Customer> customers = new HashSet<>();
        Customer customer1 = new Customer();
        customer1.setFirstName("firstName1");
        customer1.setLastName("lastName1");
        customers.add(customer1);

        Customer customer2 = new Customer();
        customer2.setFirstName("firstName2");
        customer2.setLastName("lastName2");
        customers.add(customer2);

        Customer customer3 = new Customer();
        customer3.setFirstName("firstName3");
        customer3.setLastName("lastName3");
        customers.add(customer3);

        customerRepository.saveAll(customers);
        log.info("Data loaded! Number of customers: " + customerRepository.count());
    }

    private void loadCategories() {
        Set<Category> categories = new HashSet<>();

        Category fruits = new Category();
        fruits.setName("Fruits");
        categories.add(fruits);

        Category Dried = new Category();
        Dried.setName("Dried");
        categories.add(Dried);

        Category Fresh = new Category();
        Fresh.setName("Fresh");
        categories.add(Fresh);

        Category Exotic = new Category();
        Exotic.setName("Exotic");
        categories.add(Exotic);

        Category Nuts = new Category();
        Nuts.setName("Nuts");
        categories.add(Nuts);

        categoryRepository.saveAll(categories);
        log.info("Data loaded! Number of categories: " + categoryRepository.count());
    }
}
