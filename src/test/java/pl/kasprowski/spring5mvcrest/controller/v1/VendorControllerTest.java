package pl.kasprowski.spring5mvcrest.controller.v1;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.spring5mvcrest.api.v1.model.VendorDto;
import pl.kasprowski.spring5mvcrest.exceptions.ResourceNotFoundException;
import pl.kasprowski.spring5mvcrest.services.VendorService;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class VendorControllerTest extends AbstractRestControllerTest {

    @InjectMocks
    private VendorController vendorController;
    @Mock
    private VendorService vendorService;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(vendorController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    @Test
    public void getAllVendors() throws Exception {
        when(vendorService.getAllVendors()).thenReturn(Arrays.asList(new VendorDto(), new VendorDto()));

        mockMvc.perform(get("/api/v1/vendors")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.vendors", hasSize(2)));

        verify(vendorService, times(1)).getAllVendors();
    }

    @Test
    public void createNewVendor() throws Exception {
        VendorDto dto = new VendorDto();
        dto.setName("name");
        VendorDto savedDto = new VendorDto();
        savedDto.setName("name");
        savedDto.setVendorUrl("/api/v1/vendors/1");
        when(vendorService.createNewVendor(any(VendorDto.class))).thenReturn(savedDto);

        mockMvc.perform(post("/api/v1/vendors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", equalTo("name")))
                .andExpect(jsonPath("$.vendorUrl", equalTo("/api/v1/vendors/1")));

        verify(vendorService, times(1)).createNewVendor(eq(dto));
    }

    @Test
    public void deleteVendorById() throws Exception {
        Long id = 1L;

        mockMvc.perform(delete("/api/v1/vendors/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(vendorService, times(1)).deleteVendorById(eq(id));
    }

    @Test
    public void getVendorById() throws Exception {
        Long id = 1L;
        VendorDto dto = new VendorDto();
        dto.setName("name");
        dto.setVendorUrl("/api/v1/vendors/1");
        when(vendorService.getVendorById(anyLong())).thenReturn(dto);

        mockMvc.perform(get("/api/v1/vendors/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("name")))
                .andExpect(jsonPath("$.vendorUrl", equalTo("/api/v1/vendors/1")));

        verify(vendorService, times(1)).getVendorById(eq(id));
    }

    @Test
    public void patchVendor() throws Exception {
        Long id = 1L;
        VendorDto dto = new VendorDto();
        dto.setName("name");
        VendorDto savedDto = new VendorDto();
        savedDto.setName("name");
        savedDto.setVendorUrl("/api/v1/vendors/1");
        when(vendorService.patchVendor(anyLong(), any(VendorDto.class))).thenReturn(savedDto);

        mockMvc.perform(patch("/api/v1/vendors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("name")))
                .andExpect(jsonPath("$.vendorUrl", equalTo("/api/v1/vendors/1")));


        verify(vendorService, times(1)).patchVendor(eq(id), eq(dto));
    }

    @Test
    public void updateVendor() throws Exception {
        Long id = 1L;
        VendorDto dto = new VendorDto();
        dto.setName("name");
        VendorDto savedDto = new VendorDto();
        savedDto.setName("name");
        savedDto.setVendorUrl("/api/v1/vendors/1");
        when(vendorService.updateVendor(anyLong(), any(VendorDto.class))).thenReturn(savedDto);

        mockMvc.perform(put("/api/v1/vendors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("name")))
                .andExpect(jsonPath("$.vendorUrl", equalTo("/api/v1/vendors/1")));

        verify(vendorService, times(1)).updateVendor(eq(id), eq(dto));
    }

    @Test
    public void getVendorById_NotFound() throws Exception {
        when(vendorService.getVendorById(anyLong())).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(get("/api/v1/vendors/123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}