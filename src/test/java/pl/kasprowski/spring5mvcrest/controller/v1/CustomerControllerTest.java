package pl.kasprowski.spring5mvcrest.controller.v1;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.spring5mvcrest.api.v1.model.CustomerDto;
import pl.kasprowski.spring5mvcrest.exceptions.ResourceNotFoundException;
import pl.kasprowski.spring5mvcrest.services.CustomerService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest extends AbstractRestControllerTest {

    @InjectMocks
    private CustomerController customerController;
    @Mock
    private CustomerService customerService;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(customerController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    @Test
    public void getAllCustomersTest() throws Exception {
        CustomerDto dto1 = new CustomerDto();
        dto1.setFirstName("firstName1");
        dto1.setLastName("lastName1");
        dto1.setCustomerUrl("/shop/customer/1");

        CustomerDto dto2 = new CustomerDto();
        dto2.setFirstName("firstName2");
        dto2.setLastName("lastName2");
        dto2.setCustomerUrl("/shop/customer/2");

        List<CustomerDto> dtos = Arrays.asList(dto1, dto2);
        when(customerService.getAllCustomers()).thenReturn(dtos);

        mockMvc.perform(get("/api/v1/customers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customers", hasSize(2)));

        verify(customerService, times(1)).getAllCustomers();
    }

    @Test
    public void getCustomerByIdTest() throws Exception {
        CustomerDto dto = new CustomerDto();
        dto.setFirstName("firstName");
        dto.setLastName("lastName");
        dto.setCustomerUrl("/shop/customer/2");
        when(customerService.getCustomerById(anyLong())).thenReturn(dto);

        mockMvc.perform(get("/api/v1/customers/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", equalTo("firstName")));

        verify(customerService, times(1)).getCustomerById(anyLong());
    }

    @Test
    public void createNewCustomer() throws Exception {
        CustomerDto dto = new CustomerDto();
        dto.setFirstName("firstName");
        dto.setLastName("lastName");
        CustomerDto savedDto = new CustomerDto();
        savedDto.setFirstName("firstName");
        savedDto.setLastName("lastName");
        savedDto.setCustomerUrl("/shop/customer/2");

        when(customerService.createNewCustomer(any(CustomerDto.class))).thenReturn(savedDto);

        mockMvc.perform(post("/api/v1/customers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName", equalTo("firstName")))
                .andExpect(jsonPath("$.customerUrl", equalTo("/shop/customer/2")));

        verify(customerService, times(1)).createNewCustomer(any());
    }

    @Test
    public void updateCustomer() throws Exception {
        CustomerDto dto = new CustomerDto();
        dto.setFirstName("firstName");
        dto.setLastName("lastName");
        CustomerDto savedDto = new CustomerDto();
        savedDto.setFirstName("firstName");
        savedDto.setLastName("lastName");
        savedDto.setCustomerUrl("/shop/customer/2");

        when(customerService.updateCustomer(anyLong(), any(CustomerDto.class))).thenReturn(savedDto);

        mockMvc.perform(put("/api/v1/customers/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", equalTo("firstName")))
                .andExpect(jsonPath("$.customerUrl", equalTo("/shop/customer/2")));

        verify(customerService, times(1)).updateCustomer(anyLong(), any(CustomerDto.class));
    }

    @Test
    public void patchCustomer() throws Exception {
        CustomerDto dto = new CustomerDto();
        dto.setFirstName("firstName");
        CustomerDto savedDto = new CustomerDto();
        savedDto.setFirstName("firstName");
        savedDto.setLastName("lastName");
        savedDto.setCustomerUrl("/shop/customer/2");

        when(customerService.patchCustomer(anyLong(), any(CustomerDto.class))).thenReturn(savedDto);

        mockMvc.perform(patch("/api/v1/customers/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", equalTo("firstName")));

        verify(customerService, times(1)).patchCustomer(anyLong(), any(CustomerDto.class));
    }

    @Test
    public void deleteCustomerById() throws Exception {
        mockMvc.perform(delete("/api/v1/customers/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(customerService, times(1)).deleteCustomerById(anyLong());
    }

    @Test
    public void getCustomerByIdTest_NotFound() throws Exception {
        when(customerService.getCustomerById(anyLong())).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(get("/api/v1/customers/123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}