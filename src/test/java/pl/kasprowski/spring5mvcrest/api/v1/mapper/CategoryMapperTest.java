package pl.kasprowski.spring5mvcrest.api.v1.mapper;

import org.junit.Test;
import pl.kasprowski.spring5mvcrest.api.v1.model.CategoryDto;
import pl.kasprowski.spring5mvcrest.entity.Category;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CategoryMapperTest {

    private static final Long ID = 1L;
    private static final String NAME = "name";
    private CategoryMapper mapper = CategoryMapper.INSTANCE;

    @Test
    public void categoryToDto() throws Exception {
        Category category = new Category();
        category.setName(NAME);
        category.setId(ID);

        CategoryDto dto = mapper.categoryToDto(category);

        assertNotNull(dto);
        assertEquals(ID, dto.getId());
        assertEquals(NAME, dto.getName());
    }

    @Test
    public void categoriesToDtos() throws Exception {
        Category category = new Category();
        category.setName(NAME);
        category.setId(ID);

        Category category2 = new Category();
        category2.setName(NAME);
        category2.setId(ID + 1);

        List<Category> categories = Arrays.asList(category, category2);

        List<CategoryDto> dtos = mapper.categoriesToDtos(categories);

        assertNotNull(dtos);
        assertEquals(2, dtos.size());
        CategoryDto dto = dtos.get(0);
        assertEquals(ID, dto.getId());
        assertEquals(NAME, dto.getName());
    }
}