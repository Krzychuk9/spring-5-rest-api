package pl.kasprowski.spring5mvcrest.api.v1.mapper;

import org.junit.Test;
import pl.kasprowski.spring5mvcrest.api.v1.model.CustomerDto;
import pl.kasprowski.spring5mvcrest.entity.Customer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CustomerMapperTest {

    private static final Long ID = 1L;
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String CUSTOMER_URL = "/shop/customer/" + ID;
    private CustomerMapper mapper = CustomerMapper.INSTANCE;


    @Test
    public void customerToDto() throws Exception {
        Customer customer = new Customer();
        customer.setFirstName(FIRST_NAME);
        customer.setLastName(LAST_NAME);
        customer.setId(ID);

        CustomerDto dto = mapper.customerToDto(customer);

        assertNotNull(dto);
        assertEquals(FIRST_NAME, dto.getFirstName());
        assertEquals(LAST_NAME, dto.getLastName());
        assertEquals(CUSTOMER_URL, dto.getCustomerUrl());
    }

    @Test
    public void customersToDtos() throws Exception {
        Customer customer = new Customer();
        customer.setFirstName(FIRST_NAME);
        customer.setLastName(LAST_NAME);
        customer.setId(ID);

        Customer customer2 = new Customer();
        customer2.setFirstName(FIRST_NAME);
        customer2.setLastName(LAST_NAME);
        customer2.setId(ID + 1);

        List<Customer> customers = Arrays.asList(customer, customer2);

        List<CustomerDto> dtos = mapper.customersToDtos(customers);

        assertNotNull(dtos);
        assertEquals(2, dtos.size());
        CustomerDto dto = dtos.get(0);
        assertEquals(FIRST_NAME, dto.getFirstName());
        assertEquals(LAST_NAME, dto.getLastName());
        assertEquals(CUSTOMER_URL, dto.getCustomerUrl());
    }

    @Test
    public void dtoToCustomer() throws Exception {
        CustomerDto dto = new CustomerDto();
        dto.setFirstName(FIRST_NAME);
        dto.setLastName(LAST_NAME);

        Customer customer = mapper.dtoToCustomer(dto);

        assertNotNull(customer);
        assertEquals(FIRST_NAME, customer.getFirstName());
        assertEquals(LAST_NAME, customer.getLastName());
    }

    @Test
    public void dtosToCustomers() throws Exception {
        CustomerDto dto1 = new CustomerDto();
        dto1.setFirstName(FIRST_NAME);
        dto1.setLastName(LAST_NAME);

        CustomerDto dto2 = new CustomerDto();
        dto2.setFirstName(FIRST_NAME);
        dto2.setLastName(LAST_NAME);

        List<CustomerDto> dtos = Arrays.asList(dto1, dto2);

        List<Customer> customers = mapper.dtosToCustomer(dtos);

        assertNotNull(customers);
        assertEquals(2, customers.size());
        Customer customer = customers.get(0);
        assertEquals(FIRST_NAME, customer.getFirstName());
        assertEquals(LAST_NAME, customer.getLastName());
    }
}