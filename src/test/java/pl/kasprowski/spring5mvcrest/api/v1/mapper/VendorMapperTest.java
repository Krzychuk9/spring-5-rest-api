package pl.kasprowski.spring5mvcrest.api.v1.mapper;

import org.junit.Test;
import pl.kasprowski.spring5mvcrest.api.v1.model.VendorDto;
import pl.kasprowski.spring5mvcrest.entity.Vendor;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class VendorMapperTest {

    private static final Long ID = 1L;
    private static final String NAME = "Fruits vendor";
    private static final String API_URL_WITH_ID = "/api/v1/vendors/1";

    private VendorMapper mapper = VendorMapper.INSTANCE;

    @Test
    public void vendorToDto() throws Exception {
        Vendor vendor = new Vendor();
        vendor.setId(ID);
        vendor.setName(NAME);

        VendorDto dto = mapper.vendorToDto(vendor);

        assertNotNull(dto);
        assertEquals(NAME, dto.getName());
        assertEquals(API_URL_WITH_ID, dto.getVendorUrl());
    }

    @Test
    public void dtoToVendor() throws Exception {
        VendorDto dto = new VendorDto();
        dto.setName(NAME);
        dto.setVendorUrl(API_URL_WITH_ID);

        Vendor vendor = mapper.dtoToVendor(dto);

        assertNotNull(vendor);
        assertEquals(NAME, vendor.getName());
    }

    @Test
    public void vendrosToDtos() throws Exception {
        Vendor vendor = new Vendor();
        vendor.setId(ID);
        vendor.setName(NAME);

        Vendor vendor2 = new Vendor();
        vendor2.setId(2L);
        vendor2.setName(NAME);

        List<VendorDto> dtos = mapper.vendrosToDtos(Arrays.asList(vendor, vendor2));

        assertNotNull(dtos);
        assertEquals(2, dtos.size());
        VendorDto dto = dtos.get(0);
        assertNotNull(dto);
        assertEquals(NAME, dto.getName());
        assertEquals(API_URL_WITH_ID, dto.getVendorUrl());
    }

    @Test
    public void dtosToVendros() throws Exception {
        VendorDto dto = new VendorDto();
        dto.setName(NAME);
        dto.setVendorUrl(API_URL_WITH_ID);

        VendorDto dto2 = new VendorDto();
        dto2.setName(NAME);
        dto2.setVendorUrl(API_URL_WITH_ID);

        List<Vendor> vendors = mapper.dtosToVendros(Arrays.asList(dto, dto2));

        assertNotNull(vendors);
        assertEquals(2, vendors.size());
        Vendor vendor = vendors.get(0);
        assertNotNull(vendor);
        assertEquals(NAME, vendor.getName());
    }
}