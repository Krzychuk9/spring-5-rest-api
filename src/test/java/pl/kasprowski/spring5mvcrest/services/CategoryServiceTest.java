package pl.kasprowski.spring5mvcrest.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kasprowski.spring5mvcrest.api.v1.mapper.CategoryMapper;
import pl.kasprowski.spring5mvcrest.api.v1.model.CategoryDto;
import pl.kasprowski.spring5mvcrest.entity.Category;
import pl.kasprowski.spring5mvcrest.repositories.CategoryRepository;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {

    private static final Long ID = 2L;
    private static final String NAME = "Joy";

    private CategoryService categoryService;
    @Mock
    private CategoryRepository categoryRepository;

    @Before
    public void setUp() throws Exception {
        categoryService = new CategoryServiceImpl(categoryRepository, CategoryMapper.INSTANCE);
    }

    @Test
    public void getCategoryByName() throws Exception {
        Category category = new Category();
        category.setId(ID);
        category.setName(NAME);
        when(categoryRepository.findByName(anyString())).thenReturn(category);

        CategoryDto dto = categoryService.getCategoryByName(NAME);

        assertNotNull(dto);
        assertEquals(ID, dto.getId());
        assertEquals(NAME, dto.getName());
    }

    @Test
    public void getAllCategories() throws Exception {
        List<Category> categories = Arrays.asList(new Category(), new Category(), new Category());
        when(categoryRepository.findAll()).thenReturn(categories);

        List<CategoryDto> dtos = categoryService.getAllCategories();

        assertNotNull(dtos);
        assertEquals(3, dtos.size());
    }
}