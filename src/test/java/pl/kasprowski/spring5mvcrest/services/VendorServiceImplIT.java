package pl.kasprowski.spring5mvcrest.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.kasprowski.spring5mvcrest.api.v1.mapper.VendorMapper;
import pl.kasprowski.spring5mvcrest.api.v1.model.VendorDto;
import pl.kasprowski.spring5mvcrest.bootstrap.Bootstrap;
import pl.kasprowski.spring5mvcrest.entity.Vendor;
import pl.kasprowski.spring5mvcrest.exceptions.ResourceNotFoundException;
import pl.kasprowski.spring5mvcrest.repositories.CategoryRepository;
import pl.kasprowski.spring5mvcrest.repositories.CustomerRepository;
import pl.kasprowski.spring5mvcrest.repositories.VendorRepository;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class VendorServiceImplIT {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private VendorRepository vendorRepository;
    private VendorService vendorService;

    @Before
    public void setUp() throws Exception {
        Bootstrap bootstrap = new Bootstrap(categoryRepository, customerRepository, vendorRepository);
        bootstrap.run();
        vendorService = new VendorServiceImpl(vendorRepository, VendorMapper.INSTANCE);
    }

    @Test
    public void patchVendor() throws Exception {
        String updateName = "UpdateName";
        Long id = this.getVendorIdValue();

        Vendor oldVendor = vendorRepository.findById(id).get();
        assertNotNull(oldVendor);
        String oldName = oldVendor.getName();

        VendorDto dto = new VendorDto();
        dto.setName(updateName);

        vendorService.patchVendor(id, dto);

        Vendor updatedVendor = vendorRepository.findById(id).get();

        assertNotNull(updatedVendor);
        assertEquals(updateName, updatedVendor.getName());
        assertThat(oldName, not(equalTo(updatedVendor.getName())));
    }

    @Test
    public void patchVendor_emptyName() throws Exception {
        Long id = this.getVendorIdValue();

        Vendor oldVendor = vendorRepository.findById(id).get();
        assertNotNull(oldVendor);
        String oldName = oldVendor.getName();

        VendorDto dto = new VendorDto();
        vendorService.patchVendor(id, dto);

        Vendor updatedVendor = vendorRepository.findById(id).get();

        assertNotNull(updatedVendor);
        assertEquals(oldName, updatedVendor.getName());
    }

    private Long getVendorIdValue() {
        return vendorRepository.findAll().stream().map(Vendor::getId).findFirst().orElseThrow(ResourceNotFoundException::new);
    }
}
