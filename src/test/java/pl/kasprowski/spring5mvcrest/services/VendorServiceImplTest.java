package pl.kasprowski.spring5mvcrest.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kasprowski.spring5mvcrest.api.v1.mapper.VendorMapper;
import pl.kasprowski.spring5mvcrest.api.v1.model.VendorDto;
import pl.kasprowski.spring5mvcrest.entity.Vendor;
import pl.kasprowski.spring5mvcrest.repositories.VendorRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VendorServiceImplTest {

    private static final Long ID = 1L;
    private static final String NAME = "Fruits  Vendor";
    private static final String VENDOR_URL = "/api/v1/vendors/" + ID;

    private VendorService service;
    @Mock
    private VendorRepository vendorRepository;

    @Before
    public void setUp() throws Exception {
        service = new VendorServiceImpl(vendorRepository, VendorMapper.INSTANCE);
    }

    @Test
    public void getVendorById() throws Exception {
        Vendor vendor = new Vendor();
        vendor.setName(NAME);
        vendor.setId(ID);

        given(vendorRepository.findById(anyLong())).willReturn(Optional.of(vendor));

        VendorDto dto = service.getVendorById(ID);

        then(vendorRepository).should(times(1)).findById(eq(ID));

        assertNotNull(dto);
        assertEquals(NAME, dto.getName());
        assertEquals(VENDOR_URL, dto.getVendorUrl());
    }

    @Test
    public void getAllVendors() throws Exception {
        List<Vendor> vendors = Arrays.asList(new Vendor(), new Vendor(), new Vendor());
        when(vendorRepository.findAll()).thenReturn(vendors);

        List<VendorDto> dtos = service.getAllVendors();

        assertNotNull(dtos);
        assertEquals(3, dtos.size());
        verify(vendorRepository, times(1)).findAll();
    }

    @Test
    public void createNewVendor() throws Exception {
        VendorDto dto = new VendorDto();
        dto.setName(NAME);
        Vendor savedVendor = new Vendor();
        savedVendor.setName(NAME);
        savedVendor.setId(ID);
        when(vendorRepository.save(any(Vendor.class))).thenReturn(savedVendor);

        VendorDto vendor = service.createNewVendor(dto);

        assertNotNull(vendor);
        assertEquals(NAME, vendor.getName());
        verify(vendorRepository, times(1)).save(any(Vendor.class));
    }

    @Test
    public void updateVendor() throws Exception {
        VendorDto dto = new VendorDto();
        dto.setName(NAME);
        Vendor savedVendor = new Vendor();
        savedVendor.setName(NAME);
        savedVendor.setId(ID);
        when(vendorRepository.save(any(Vendor.class))).thenReturn(savedVendor);

        VendorDto updatedVendor = service.updateVendor(ID, dto);

        assertNotNull(updatedVendor);
        assertEquals(NAME, updatedVendor.getName());
        assertEquals(VENDOR_URL, updatedVendor.getVendorUrl());
        verify(vendorRepository, times(1)).save(any(Vendor.class));
    }

    @Test
    public void deleteVendorById() throws Exception {
        service.deleteVendorById(ID);
        verify(vendorRepository, times(1)).deleteById(anyLong());
    }

}