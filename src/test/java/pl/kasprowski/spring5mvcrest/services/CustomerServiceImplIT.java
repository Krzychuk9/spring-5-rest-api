package pl.kasprowski.spring5mvcrest.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.kasprowski.spring5mvcrest.api.v1.mapper.CustomerMapper;
import pl.kasprowski.spring5mvcrest.api.v1.model.CustomerDto;
import pl.kasprowski.spring5mvcrest.bootstrap.Bootstrap;
import pl.kasprowski.spring5mvcrest.entity.Customer;
import pl.kasprowski.spring5mvcrest.exceptions.ResourceNotFoundException;
import pl.kasprowski.spring5mvcrest.repositories.CategoryRepository;
import pl.kasprowski.spring5mvcrest.repositories.CustomerRepository;
import pl.kasprowski.spring5mvcrest.repositories.VendorRepository;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerServiceImplIT {

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    VendorRepository vendorRepository;

    CustomerService customerService;

    @Before
    public void setUp() throws Exception {
        Bootstrap bootstrap = new Bootstrap(categoryRepository, customerRepository, vendorRepository);
        bootstrap.run();
        customerService = new CustomerServiceImpl(customerRepository, CustomerMapper.INSTANCE);
    }

    @Test
    public void patchCustomerUpdateFirstName() throws Exception {
        String updatedName = "UpdatedName";
        long id = this.getCustomerIdValue();

        Customer customer = customerRepository.findById(id).get();
        assertNotNull(customer);

        String firstName = customer.getFirstName();
        String lastName = customer.getLastName();

        CustomerDto dto = new CustomerDto();
        dto.setFirstName(updatedName);

        customerService.patchCustomer(id, dto);

        Customer updatedCustomer = customerRepository.findById(id).get();

        assertNotNull(updatedCustomer);
        assertEquals(updatedName, updatedCustomer.getFirstName());
        assertThat(firstName, not(equalTo(updatedCustomer.getFirstName())));
        assertThat(lastName, equalTo(updatedCustomer.getLastName()));
    }

    @Test
    public void patchCustomerUpdateLastName() throws Exception {
        String updatedName = "UpdatedLastName";
        Long id = this.getCustomerIdValue();

        Customer customer = customerRepository.findById(id).get();
        assertNotNull(customer);

        String firstName = customer.getFirstName();
        String lastName = customer.getLastName();

        CustomerDto dto = new CustomerDto();
        dto.setLastName(updatedName);

        customerService.patchCustomer(id, dto);

        Customer updatedCustomer = customerRepository.findById(id).get();

        assertNotNull(updatedCustomer);
        assertEquals(updatedName, updatedCustomer.getLastName());
        assertThat(firstName, equalTo(updatedCustomer.getFirstName()));
        assertThat(lastName, not(equalTo(updatedCustomer.getLastName())));
    }

    private Long getCustomerIdValue() {
        return customerRepository.findAll().stream().map(Customer::getId).findFirst().orElseThrow(ResourceNotFoundException::new);
    }
}
