package pl.kasprowski.spring5mvcrest.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kasprowski.spring5mvcrest.api.v1.mapper.CustomerMapper;
import pl.kasprowski.spring5mvcrest.api.v1.model.CustomerDto;
import pl.kasprowski.spring5mvcrest.entity.Customer;
import pl.kasprowski.spring5mvcrest.repositories.CustomerRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    private static final Long ID = 1L;
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String CUSTOMER_URL = "/shop/customer/" + ID;

    private CustomerService customerService;
    @Mock
    private CustomerRepository customerRepository;

    @Before
    public void setUp() throws Exception {
        customerService = new CustomerServiceImpl(customerRepository, CustomerMapper.INSTANCE);
    }

    @Test
    public void getCustomerById() throws Exception {
        Customer customer = new Customer();
        customer.setId(ID);
        customer.setFirstName(FIRST_NAME);
        customer.setLastName(LAST_NAME);
        when(customerRepository.findById(anyLong())).thenReturn(Optional.of(customer));

        CustomerDto dto = customerService.getCustomerById(ID);

        assertNotNull(dto);
        assertEquals(FIRST_NAME, dto.getFirstName());
        assertEquals(LAST_NAME, dto.getLastName());
        assertEquals(CUSTOMER_URL, dto.getCustomerUrl());
    }

    @Test
    public void getAllCustomers() throws Exception {
        List<Customer> customers = Arrays.asList(new Customer(), new Customer(), new Customer());
        when(customerRepository.findAll()).thenReturn(customers);

        List<CustomerDto> dtos = customerService.getAllCustomers();

        assertNotNull(dtos);
        assertEquals(3, dtos.size());
    }

    @Test
    public void createNewCustomer() throws Exception {
        CustomerDto dto = new CustomerDto();
        dto.setFirstName(FIRST_NAME);
        dto.setLastName(LAST_NAME);
        Customer savedCustomer = new Customer();
        savedCustomer.setId(ID);
        savedCustomer.setFirstName(FIRST_NAME);
        savedCustomer.setLastName(LAST_NAME);

        when(customerRepository.save(any(Customer.class))).thenReturn(savedCustomer);

        CustomerDto customer = customerService.createNewCustomer(dto);

        assertNotNull(customer);
        assertEquals(FIRST_NAME, customer.getFirstName());
        assertEquals(LAST_NAME, customer.getLastName());
        assertEquals(CUSTOMER_URL, customer.getCustomerUrl());
    }

    @Test
    public void updateCustomer() throws Exception {
        CustomerDto dto = new CustomerDto();
        dto.setFirstName(FIRST_NAME);
        dto.setLastName(LAST_NAME);
        Customer savedCustomer = new Customer();
        savedCustomer.setId(ID);
        savedCustomer.setFirstName(FIRST_NAME);
        savedCustomer.setLastName(LAST_NAME);

        when(customerRepository.save(any(Customer.class))).thenReturn(savedCustomer);

        CustomerDto customer = customerService.updateCustomer(ID, dto);

        assertNotNull(customer);
        assertEquals(FIRST_NAME, customer.getFirstName());
        assertEquals(LAST_NAME, customer.getLastName());
        assertEquals(CUSTOMER_URL, customer.getCustomerUrl());
    }

    @Test
    public void deleteCustomerById() throws Exception {
        Long id = 1L;

        customerService.deleteCustomerById(id);

        verify(customerRepository, times(1)).deleteById(anyLong());
    }
}